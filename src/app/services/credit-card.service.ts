import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Person } from '../interfaces/person';
import { Card } from '../interfaces/card';

@Injectable({
  providedIn: 'root'
})
export class CreditCardService {

  domain = 'http://localhost:3000/';

  constructor(private http: HttpClient) { }

  getClients(): Observable<Person[]> {
    return this.http.post<Person[]>(this.domain + 'person/list', {});
  }

  queryDetails(id: number): Observable<Card[]> {
    return this.http.post<Card[]>(this.domain + 'card/query', {person: id});
  }

  addCard(card: string, id: number): Observable<Card>{
    return this.http.post<Card>(this.domain + 'card/add', {person_id: id, card_number: card});
  }
}
