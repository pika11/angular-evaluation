import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { CreditCardService } from '../services/credit-card.service';
import { Observable } from 'rxjs';
import { Card } from '../interfaces/card';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  creditCards: Observable<Card[]>;
  newCard = '';

  constructor(private route: ActivatedRoute, private creditCardService: CreditCardService) { }

  ngOnInit() {
    this.loadCards();
  }

  loadCards() {
    this.creditCards = this.route.paramMap.pipe(
      switchMap(
        (params: ParamMap) => this.creditCardService.queryDetails(Number(params.get('id')))
      )
    );
  }

  addCard() {
    if (this.newCard === '') {
      return;
    }
    this.creditCardService.addCard(this.newCard, Number(this.route.snapshot.paramMap.get('id')))
      .subscribe(
        card => {
          this.newCard = '';
          this.loadCards();
        }
      );
  }

}
