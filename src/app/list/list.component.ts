import { Component, OnInit } from '@angular/core';
import { CreditCardService } from '../services/credit-card.service';
import { Person } from '../interfaces/person';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  persons: Observable<Person[]>;

  constructor(private creditCardService: CreditCardService) { }

  ngOnInit() {
    this.persons = this.creditCardService.getClients();
  }

}
