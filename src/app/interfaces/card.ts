export interface Card {
    person_id: number;
    card_number: string;
    id?: number;
    balance?: number;
}
